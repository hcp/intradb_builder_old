/* 
 * org.nrg.pipeline.exception.PipelineException
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 * 	
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 8:47 PM
*/

package org.nrg.pipeline.exception;

@SuppressWarnings("serial")
public class PipelineException extends Exception {
    public PipelineException() {
        super();
    }
}
