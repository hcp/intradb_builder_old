/*
 * org.nrg.xnat.helpers.uri.archive.AssessedURII
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 8:47 PM
 */
package org.nrg.xnat.helpers.uri.archive;

import org.nrg.xdat.om.XnatImagesessiondata;

public interface AssessedURII {
	public XnatImagesessiondata getSession();
}
