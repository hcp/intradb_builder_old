/*
 * org.nrg.xnat.helpers.uri.archive.ExperimentURII
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 8:47 PM
 */
package org.nrg.xnat.helpers.uri.archive;

import org.nrg.xdat.om.XnatExperimentdata;

public interface ExperimentURII {
	public XnatExperimentdata getExperiment();
}
