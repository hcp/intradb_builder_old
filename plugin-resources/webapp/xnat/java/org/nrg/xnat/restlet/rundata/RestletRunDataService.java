/*
 * org.nrg.xnat.restlet.rundata.RestletRunDataService
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 8:40 PM
 */
package org.nrg.xnat.restlet.rundata;

import org.apache.turbine.services.rundata.TurbineRunDataService;

public class RestletRunDataService extends TurbineRunDataService {

}
