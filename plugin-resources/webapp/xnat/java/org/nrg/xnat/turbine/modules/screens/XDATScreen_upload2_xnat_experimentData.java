/*
 * org.nrg.xnat.turbine.modules.screens.XDATScreen_upload2_xnat_experimentData
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 8:47 PM
 */
package org.nrg.xnat.turbine.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureReport;

public class XDATScreen_upload2_xnat_experimentData extends SecureReport {

	@Override
	public void finalProcessing(RunData data, Context context) {
		
	}

}
