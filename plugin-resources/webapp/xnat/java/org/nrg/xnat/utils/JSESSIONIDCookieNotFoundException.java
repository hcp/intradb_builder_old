/*
 * org.nrg.xnat.utils.JSESSIONIDCookieNotFoundException
 * XNAT http://www.xnat.org
 * Copyright (c) 2013, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 8:47 PM
 */
package org.nrg.xnat.utils;

public final class JSESSIONIDCookieNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -1647291274342602813L;

}
